package com.example.inspiringpeople.interfaces

interface InspiringPersonInteractionListener {
    fun onImageClick(quotes: List<String>)
}