package com.example.inspiringpeople.models

import java.util.*

data class InspiringPerson(
    var name: String = "",
    var description: String = "",
    var birthDate: Date = Date(),
    var imageURI: String = "",
    var quotes: List<String> = listOf()
) {

    var deathDate: Date? = null

    constructor(
        name: String,
        description: String,
        birthDate: Date,
        deathDate: Date,
        imageURI: String,
        quotes: List<String>
    ) : this(name, description, birthDate, imageURI, quotes) {
        this.deathDate = deathDate
    }
}