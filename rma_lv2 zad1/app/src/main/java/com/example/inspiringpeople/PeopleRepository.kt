package com.example.inspiringpeople

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.inspiringpeople.models.InspiringPerson
import java.util.*

object PeopleRepository {
    private var _people = MutableLiveData(mutableListOf<InspiringPerson>())
    val people: LiveData<MutableList<InspiringPerson>>
        get() = _people

    init {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, 1971)
        calendar.set(Calendar.MONTH, 5)
        calendar.set(Calendar.DAY_OF_MONTH, 28)
        this._people.value?.add(
            InspiringPerson(
                "Elon Musk",
                " He is the founder, CEO, CTO, and chief designer of SpaceX; early investor,[b] CEO, and product architect of Tesla, Inc.; founder of The Boring Company; co-founder of Neuralink; and co-founder and initial co-chairman of OpenAI.",
                calendar.time,
                "https://upload.wikimedia.org/wikipedia/commons/8/85/Elon_Musk_Royal_Society_%28crop1%29.jpg",
                listOf("I think it is possible for ordinary people to choose to be extraordinary.")
            )
        )
    }

    fun add(person: InspiringPerson) {
        this._people.value?.add(person)
        this._people.notifyObserver()
    }
}

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}