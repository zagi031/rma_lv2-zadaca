package com.example.inspiringpersonszad2.interfaces

interface InspiringPersonInteractionListener {
    fun onImageClick(quotes: List<String>)
    fun onPersonClick(id: Long)
}