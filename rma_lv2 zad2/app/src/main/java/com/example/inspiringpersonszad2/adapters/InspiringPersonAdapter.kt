package com.example.inspiringpersonszad2.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.inspiringpersonszad2.R
import com.example.inspiringpersonszad2.interfaces.InspiringPersonInteractionListener
import com.example.inspiringpersonszad2.models.InspiringPerson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.inspiringperson_holder.view.*
import java.text.SimpleDateFormat

class InspiringPersonAdapter(private val listener: InspiringPersonInteractionListener) :
    RecyclerView.Adapter<InspiringPersonHolder>() {

    private val inspiringPeople = mutableListOf<InspiringPerson>()

    fun add(inspiringPerson: InspiringPerson) { //in this apps case it is not used because of liveData, on every change on peopleRepository it is called addAll()
        inspiringPeople.add(inspiringPerson)
        this.notifyItemChanged(inspiringPeople.size - 1)
    }

    fun addAll(inspiringPersons: MutableList<InspiringPerson>) {
        this.inspiringPeople.clear()
        this.inspiringPeople.addAll(inspiringPersons)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InspiringPersonHolder =
        InspiringPersonHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.inspiringperson_holder, parent, false)
        )

    override fun onBindViewHolder(holder: InspiringPersonHolder, position: Int) =
        holder.bind(this.inspiringPeople[position], listener)

    override fun getItemCount() = this.inspiringPeople.size

}

class InspiringPersonHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun bind(inspiringPerson: InspiringPerson, listener: InspiringPersonInteractionListener) {
        itemView.tv_name.text = inspiringPerson.name
        itemView.tv_Description.text = inspiringPerson.description

        itemView.tv_BirthDate.text =
            "${SimpleDateFormat("dd MMM yyyy").format(inspiringPerson.birthDate.time)} - ..."
        if (inspiringPerson.deathDate != null) {
            itemView.tv_BirthDate.text =
                SimpleDateFormat("dd MMM yyyy").format(inspiringPerson.birthDate.time) + " - ${
                    SimpleDateFormat("dd MMM yyyy").format(inspiringPerson.deathDate!!.time)
                }"
        }
        if (inspiringPerson.imageURI != String()) {
            Picasso.get().load(inspiringPerson.imageURI).fit().placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image_error).into(itemView.iv_image)
        } else itemView.iv_image.setImageResource(R.drawable.ic_image)
        itemView.iv_image.setOnClickListener { listener.onImageClick(inspiringPerson.quotes) }
        itemView.setOnClickListener { listener.onPersonClick(inspiringPerson.id) }
    }

}