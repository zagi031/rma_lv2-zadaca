package com.example.inspiringpersonszad2.models

import java.util.*

data class InspiringPerson(
    var name: String = "",
    var description: String = "",
    var birthDate: Calendar = Calendar.getInstance(),
    var imageURI: String = "",
    var quotes: List<String> = listOf()
) {

    val id = System.currentTimeMillis()
    var deathDate: Calendar? = null

    constructor(
        name: String,
        description: String,
        birthDate: Calendar,
        deathDate: Calendar,
        imageURI: String,
        quotes: List<String>
    ) : this(name, description, birthDate, imageURI, quotes) {
        this.deathDate = deathDate
    }
}