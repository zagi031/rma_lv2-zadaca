package com.example.inspiringpersonszad2.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.inspiringpersonszad2.PeopleRepository
import com.example.inspiringpersonszad2.R
import com.example.inspiringpersonszad2.adapters.ViewPagerAdapter
import com.example.inspiringpersonszad2.fragments.AddNewInspiringPersonFragment
import com.example.inspiringpersonszad2.fragments.InspiringPeopleFragment
import com.example.inspiringpersonszad2.interfaces.InspiringPersonInteractionListener
import kotlinx.android.synthetic.main.activity_main.*
import java.io.Serializable

class MainActivity : AppCompatActivity(), InspiringPersonInteractionListener, Serializable {

    private val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager.adapter = viewPagerAdapter
        tabLayout.setupWithViewPager(viewPager)

        PeopleRepository.people.observe(this, {
            viewPager.setCurrentItem(0, true)
        })
    }

    override fun onImageClick(quotes: List<String>) =
        Toast.makeText(this, quotes[(quotes.indices).random()], Toast.LENGTH_SHORT).show()

    override fun onPersonClick(id: Long) {
        viewPager.setCurrentItem(1, true)
        val addNewInspiringPersonFragment =
            viewPagerAdapter.getItem(1) as AddNewInspiringPersonFragment
        addNewInspiringPersonFragment.prepareFieldsForEditing(id)
    }
}