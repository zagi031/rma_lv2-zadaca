package com.example.inspiringpersonszad2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.inspiringpersonszad2.models.InspiringPerson
import java.util.*

object PeopleRepository {
    private var _people = MutableLiveData(mutableListOf<InspiringPerson>())
    val people: LiveData<MutableList<InspiringPerson>>
        get() = _people

    init {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, 1971)
        calendar.set(Calendar.MONTH, 5)
        calendar.set(Calendar.DAY_OF_MONTH, 28)
        _people.value?.add(
            InspiringPerson(
                "Elon Musk",
                " He is the founder, CEO, CTO, and chief designer of SpaceX; early investor,[b] CEO, and product architect of Tesla, Inc.; founder of The Boring Company; co-founder of Neuralink; and co-founder and initial co-chairman of OpenAI.",
                calendar,
                "https://upload.wikimedia.org/wikipedia/commons/8/85/Elon_Musk_Royal_Society_%28crop1%29.jpg",
                listOf("I think it is possible for ordinary people to choose to be extraordinary.")
            )
        )
    }

    fun addPerson(person: InspiringPerson) {
        _people.value?.add(person)
        _people.notifyObserver()
    }

    fun removePerson(index: Int) {
        _people.value?.removeAt(index)
        _people.notifyObserver()
    }

    fun editPerson(personID: Long, person: InspiringPerson) {
        if(_people.value?.any { it.id == personID }!!) {
            val p = _people.value?.filter { it.id == personID }?.get(0)
            p?.name = person.name
            p?.description = person.description
            p?.birthDate = person.birthDate
            p?.deathDate = person.deathDate
            p?.imageURI = person.imageURI
            p?.quotes = person.quotes
            _people.notifyObserver()
        }
        else addPerson(person)
    }

    fun getPerson(personID: Long) = _people.value?.filter { it.id == personID }?.get(0)
}

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}