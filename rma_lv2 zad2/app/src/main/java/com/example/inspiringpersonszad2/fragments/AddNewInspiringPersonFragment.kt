package com.example.inspiringpersonszad2.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.inspiringpersonszad2.models.InspiringPerson
import com.example.inspiringpersonszad2.PeopleRepository
import com.example.inspiringpersonszad2.R
import kotlinx.android.synthetic.main.fragment_add_inspiring_person.*
import java.util.*

class AddNewInspiringPersonFragment : Fragment() {

    private var personID: Long? = null

    companion object {
        fun newInstance(): AddNewInspiringPersonFragment {
            return AddNewInspiringPersonFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_inspiring_person, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rg_isDead.setOnCheckedChangeListener { group, checkedId ->
            if (rbtn_isDead.isChecked) {
                tv_DeathDate.visibility = View.VISIBLE
                dp_DeathDate.visibility = View.VISIBLE
            } else {
                tv_DeathDate.visibility = View.GONE
                dp_DeathDate.visibility = View.GONE
            }
        }

        btn_AddInspiringPerson.setOnClickListener {
            val name = et_Name.text.toString().trim()
            val description = et_Description.text.toString().trim()
            val birthDate = Calendar.getInstance()
            birthDate.set(Calendar.YEAR, dp_BirthDate.year)
            birthDate.set(Calendar.MONTH, dp_BirthDate.month)
            birthDate.set(Calendar.DAY_OF_MONTH, dp_BirthDate.dayOfMonth)
            val imageURL = et_imageURL.text.toString()
            val quotesText = et_quote.text.toString().trim()
            var quotes = listOf<String>()
            if (quotesText != String()) {
                val delimiter = "."
                quotes = quotesText.split(delimiter).filter { it.isNotBlank() }
            }


            if (name != String() && description != String() && quotes.isNotEmpty()) {    //person image has placeholder in case of empty imageURL
                if (rbtn_isDead.isChecked) {
                    val deathDate = Calendar.getInstance()
                    deathDate.set(Calendar.YEAR, dp_DeathDate.year)
                    deathDate.set(Calendar.MONTH, dp_DeathDate.month)
                    deathDate.set(Calendar.DAY_OF_MONTH, dp_DeathDate.dayOfMonth)
                    if (birthDate < deathDate) {
                        val person = InspiringPerson(
                            name,
                            description,
                            birthDate,
                            deathDate,
                            imageURL,
                            quotes
                        )
                        if (personID != null) {
                            PeopleRepository.editPerson(personID!!, person)
                            personID = null
                        } else {
                            PeopleRepository.addPerson(
                                person
                            )
                        }
                        clearInputs()
                    } else Toast.makeText(
                        this.context,
                        "Check birth and death dates.",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } else {
                    val person = InspiringPerson(
                        name,
                        description,
                        birthDate,
                        imageURL,
                        quotes
                    )
                    if (personID != null) {
                        PeopleRepository.editPerson(personID!!, person)
                        personID = null
                    } else {
                        PeopleRepository.addPerson(
                            person
                        )
                    }
                    clearInputs()
                }
            } else Toast.makeText(this.context, "Some of the fields are empty.", Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun prepareFieldsForEditing(personID: Long) {
        this.personID = personID
        val inspiringPerson = PeopleRepository.getPerson(personID)
        (et_Name as TextView).text = inspiringPerson!!.name
        (et_Description as TextView).text = inspiringPerson.description
        dp_BirthDate.updateDate(
            inspiringPerson.birthDate[Calendar.YEAR],
            inspiringPerson.birthDate[Calendar.MONTH],
            inspiringPerson.birthDate[Calendar.DAY_OF_MONTH]
        )
        if (inspiringPerson.deathDate != null) {
            rbtn_isDead.isChecked = true
            dp_DeathDate.updateDate(
                inspiringPerson.deathDate!![Calendar.YEAR],
                inspiringPerson.deathDate!![Calendar.MONTH],
                inspiringPerson.deathDate!![Calendar.DAY_OF_MONTH]
            )
        } else {
            rbtn_isNotDead.isChecked = true
            val c = Calendar.getInstance()
            dp_DeathDate.updateDate(
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)
            )
        }

        (et_imageURL as TextView).text = inspiringPerson.imageURI
        var quotes = ""
        inspiringPerson.quotes.forEach {
            quotes += it
        }
        (et_quote as TextView).text = quotes
    }

    private fun clearInputs() {
        et_Name.text.clear()
        et_Description.text.clear()
        val c = Calendar.getInstance()
        dp_BirthDate.updateDate(
            c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH)
        )
        rbtn_isNotDead.isChecked = true
        dp_DeathDate.updateDate(
            c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH)
        )
        et_imageURL.text.clear()
        et_quote.text.clear()
    }
}