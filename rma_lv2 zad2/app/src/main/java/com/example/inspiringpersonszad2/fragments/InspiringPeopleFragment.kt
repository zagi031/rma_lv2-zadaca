package com.example.inspiringpersonszad2.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.inspiringpersonszad2.adapters.InspiringPersonAdapter
import com.example.inspiringpersonszad2.PeopleRepository
import com.example.inspiringpersonszad2.R
import com.example.inspiringpersonszad2.interfaces.InspiringPersonInteractionListener
import kotlinx.android.synthetic.main.fragment_inspiring_people.*
import java.io.Serializable

class InspiringPeopleFragment : Fragment() {
    private lateinit var adapter: InspiringPersonAdapter

    companion object {
        fun newInstance(): InspiringPeopleFragment {
            return InspiringPeopleFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_inspiring_people, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.adapter = InspiringPersonAdapter(activity as InspiringPersonInteractionListener)
        setupRecyclerview()
        setupListeners()
    }

    private fun setupRecyclerview() {
        rc_inspiringPeoples.layoutManager =
            LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
        rc_inspiringPeoples.addItemDecoration(
            DividerItemDecoration(
                this.context,
                RecyclerView.VERTICAL
            )
        )
        rc_inspiringPeoples.adapter = adapter

        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                AlertDialog.Builder(context)
                    .setTitle("Warning")
                    .setMessage("Really want to delete this inspiring person?")
                    .setPositiveButton(
                        "Yes"
                    ) { dialog, which -> PeopleRepository.removePerson(viewHolder.adapterPosition) }
                    .setNegativeButton(
                        "No"
                    ) { dialog, which -> adapter.notifyItemChanged(viewHolder.adapterPosition) }
                    .setOnCancelListener {
                        adapter.notifyItemChanged(viewHolder.adapterPosition)
                    }.create().show()

            }

        }).attachToRecyclerView(rc_inspiringPeoples)
    }

    private fun setupListeners() {
        PeopleRepository.people.observe(this, {
            adapter.addAll(it)
        })
    }
}