package com.example.inspiringpersonszad2.adapters

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.inspiringpersonszad2.fragments.AddNewInspiringPersonFragment
import com.example.inspiringpersonszad2.fragments.InspiringPeopleFragment
import com.example.inspiringpersonszad2.interfaces.InspiringPersonInteractionListener

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val fragments = arrayListOf(InspiringPeopleFragment.newInstance(), AddNewInspiringPersonFragment.newInstance())

    private val titles = arrayListOf("Inspiring people", "Add new inspiring person")

    override fun getCount() = titles.size

    override fun getItem(position: Int) = fragments[position]

    override fun getPageTitle(position: Int) = titles[position]
}